#if defined(LV_LVGL_H_INCLUDE_SIMPLE)
#include "lvgl.h"
#else
#include "lvgl/lvgl.h"
#endif


#ifndef LV_ATTRIBUTE_MEM_ALIGN
#define LV_ATTRIBUTE_MEM_ALIGN
#endif

#ifndef LV_ATTRIBUTE_IMG_PNISZ-(10)
#define LV_ATTRIBUTE_IMG_PNISZ-(10)
#endif

const LV_ATTRIBUTE_MEM_ALIGN LV_ATTRIBUTE_LARGE_CONST LV_ATTRIBUTE_IMG_PNISZ-(10) uint8_t PNISZ-(10)_map[] = {};

const lv_img_dsc_t PNISZ-(10) = {
  .header.cf = LV_IMG_CF_INDEXED_4BIT,
  .header.always_zero = 0,
  .header.reserved = 0,
  .header.w = 1434,
  .header.h = 612,
  .data_size = 438868,
  .data = PNISZ-(10)_map,
};
