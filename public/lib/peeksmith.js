const $ = (id) => document.getElementById(id);
const $c = (className) => document.getElementsByClassName(className);
const onClick = (el, fn) => {
  el.addEventListener("click", fn);
};
const setValue = (el, value) => {
  el.value = value;
};
const setText = (el, text) => {
  el.innerText = text;
};
const show = (el) => {
  el.style.display = "block";
};
const hide = (el) => {
  el.style.display = "none";
};
const focus = (el) => {
  el.focus();
};
const setActive = (id) => {
  const el = $(id);
  const groupClass = Array.from(el.classList).find((className) =>
    className.startsWith("group-")
  );
  for (groupEl of $c(groupClass)) {
    if (groupEl !== el) groupEl.classList.remove("active");
    else groupEl.classList.add("active");
  }
};
const onGroupClick = (groupClass, fn) => {
  for (el of $c(groupClass)) {
    el.addEventListener("click", (ev) => {
      setActive(ev.target.id);
      fn(ev.target.id.match(/\-([^-]+)$/)[1]);
    });
  }
};
const tabs = (id, active, fn) => {
  for (const tabEl of document.querySelectorAll(`#${id} .nav-link`)) {
    if (tabEl.id === active) {
      tabEl.classList.add("active");
      show($(`${tabEl.id}-content`));
    } else {
      tabEl.classList.remove("active");
      hide($(`${tabEl.id}-content`));
    }
    onClick(tabEl, (ev) => {
      fn(ev.target.id);
      for (const tabEl of document.querySelectorAll(`#${id} .nav-link`)) {
        if (ev.target != tabEl) {
          tabEl.classList.remove("active");
          hide($(`${tabEl.id}-content`));
        } else {
          tabEl.classList.add("active");
          show($(`${tabEl.id}-content`));
        }
      }
    });
  }
};

const screens = {
  initial: $("initial-screen"),
  connecting: $("connecting-screen"),
  connected: $("connected-screen"),
};
const showScreen = (screen) => {
  for (s in screens) {
    hide(screens[s]);
  }
  show(screens[screen]);
};

const deviceName = $("device-name");
const deviceVoltage = $("device-voltage");

const connectButton = $("button-connect");
const testVibrationsButton = $("button-test-vibrations");

const startDiscoveryButton = $("button-start-discovery");
const stopDiscoveryButton = $("button-stop-discovery");
const messageTextField = $("textarea-message");

const display = new PeekSmithDevice();

display.onConnecting(() => {
  showScreen("connecting");
});

display.onConnect(() => {
  showScreen("connected");
  if (display._hardwareVersion === 1) {
    setText(deviceName, `PeekSmith One #${display.id}`);
    alert("Use PeekSmith 3 with the latest firmware");
  } else if (display._hardwareVersion === 2) {
    setText(deviceName, `PeekSmith Two #${display.id}`);
    alert("Use PeekSmith 3 with the latest firmware");
  } else {
    setText(deviceName, `PeekSmith 3 #${display.id}`);
  }
  setValue(messageTextField, "");
  focus(messageTextField);
  setActive("button-ps-align-c");
  setActive("button-ps-font-au");
  setActive("button-ps-long-f");
  setActive("button-ps-scrollspeed-50");
});

display.onDisconnect(() => {
  showScreen("initial");
  setText(deviceName, "PeekSmith 3 Console");
  setText(deviceVoltage, "");
});

display.onMessage((message) => {
  console.log(`message: ${message}`);
});
display.onData((data) => {
  console.log(`data: ${data}`);
});

display.onBatteryVoltage((voltage) => {
  setText(deviceVoltage, `(${voltage / 1000}v)`);
});

onClick(connectButton, async (ev) => {
  await display.connect();
});

// TEXT TAB
const showText = () => {
  const message = messageTextField.value;
  display.displayText(message);
};
messageTextField.addEventListener("keyup", () => {
  showText();
});
onGroupClick("group-ps-align", (value) => {
  display.send(`/Ta${value}\n`);
  focus(messageTextField);
});
onGroupClick("group-ps-font", (value) => {
  display.send(`/Tf${value}\n`);
  focus(messageTextField);
  showText();
});
onGroupClick("group-ps-long", (value) => {
  display.send(`/Tw${value}\n`);
  focus(messageTextField);
  showText();
});
onGroupClick("group-ps-scrollspeed", (value) => {
  display.send(`/Ts${value}\n`);
  focus(messageTextField);
  showText();
});

// VIBRATIONS TAB
testVibrationsButton.addEventListener("click", () => {
  display.send("/VL511\n~..\n");
  setTimeout(() => {
    display.send("/VL211\n~....\n");
  }, 1500);
});

startDiscoveryButton.addEventListener("click", () => {
  display.startDiscovery();
});

stopDiscoveryButton.addEventListener("click", () => {
  display.stopDiscovery();
});

let peers = {};
display.onProxyData((message) => {
  if (message.source === null) {
    if (message.type === "list_begin") {
      peers = {};
    }
    if (message.type === "list_device") {
      peers[message.device.addr] = message.device;
    }
    if (message.type === "list_device_del") {
      delete peers[message.device.addr];
    }
    if (message.type === "list_end") {
    }
  }
  let table = "<table><th>Address</th><th>Type</th><th>Name</th>";
  for (const peer_addr of Object.keys(peers)) {
    const peer = peers[peer_addr];
    table += "<tr>";
    table += `<td>${peer.addr}</td><td>${peer.type}</td><td>${peer.name}</td>`;
    table += "</tr>";
  }
  table += "</table>";
  document.getElementById("device-table").innerHTML = table;
});

// TABS

tabs("tabs", "text-tab", (tab) => {
  if (tab === "text-tab") {
    focus(messageTextField);
  }
  if (tab === "devices-tab") {
    display.listPeers();
  }
});
